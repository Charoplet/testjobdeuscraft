﻿
public static class Constants
{
	public static class Dev
	{
		public const string DevSceneName = "Development";
	}

	public static class Axises
	{
		public const string Horizontal = "Horizontal";
		public const string Vertical = "Vertical";
	}

	public static class Tags
	{
		public const string Player = "Player";
	}
}
