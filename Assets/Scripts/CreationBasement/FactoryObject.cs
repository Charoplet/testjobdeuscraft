﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public abstract class FactoryObject<TValue> : MonoBehaviour
{
	public class Factory : Factory<TValue> { }
}

public abstract class FactoryObject<TParam1, TValue> : MonoBehaviour
{
	public class Factory : Factory<TParam1, TValue> { }
}
