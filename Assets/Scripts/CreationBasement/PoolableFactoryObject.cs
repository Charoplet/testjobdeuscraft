﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public abstract class PoolableFactoryObject<TValue> : FactoryObject<TValue> where TValue : Component
{
	public class MemoryPool : MonoMemoryPool<TValue> { }
}

public abstract class PoolableFactoryObject<TValue, TParam1> : FactoryObject<TParam1, TValue> where TValue : Component
{
	public class MemoryPool : MonoMemoryPool<TValue> { }
}
