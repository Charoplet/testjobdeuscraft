﻿using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "ScriptableObjectsInstaller", menuName = "Installers/ScriptableObjectsInstaller")]
public class ScriptableObjectsInstaller : ScriptableObjectInstaller
{
	[SerializeField]
	private GameData gameData;

	public override void InstallBindings()
	{
		Container.BindInstances(gameData);
	}
}