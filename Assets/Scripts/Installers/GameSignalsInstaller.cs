﻿using Zenject;

public class GameSignalsInstaller : Installer<GameSignalsInstaller>
{
	public override void InstallBindings()
	{
		Container.DeclareSignal<BackSignal>();
		Container.DeclareSignal<GameStartSignal>();
		Container.DeclareSignal<GamePauseSignal>();
		Container.DeclareSignal<GameQuitSignal>();
		Container.DeclareSignal<GameFocusSignal>();
		Container.DeclareSignal<LaserCountChangedSignal>();
		//todo: one signal for this??
		Container.DeclareSignal<ObjectWasDestroyedSignal<Player>>();
		Container.DeclareSignal<ObjectWasDestroyedSignal<UFO>>();
		Container.DeclareSignal<ObjectWasDestroyedSignal<Asteroid>>();
	}
}
