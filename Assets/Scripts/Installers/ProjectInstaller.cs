﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using Zenject;

public class ProjectInstaller : MonoInstaller
{
	[SerializeField]
	[InstallPrefab(typeof(RootState), true)]
	private RootState rootStatePrefab;

	[Inject]
	private GameData gameData;

	public override void InstallBindings()
	{
		GameSignalsInstaller.Install(Container);
		RecursivelyBind(this);
		BindFactories();
		BindPools();
	}

	private void RecursivelyBind(MonoBehaviour root)
	{
		if (root == null)
			return;
		foreach (var fi in GetMonoBehavioursFields(root))
		{
			var fiValue = fi.GetValue(root) as MonoBehaviour;
			var installPrefabAttribute = GetInstallPrefabAttribute(fi);
			if (installPrefabAttribute != null)
			{
				var bind = Container.Bind(installPrefabAttribute.Type).FromComponentInNewPrefab(fiValue.gameObject).AsSingle();
				if (installPrefabAttribute.NonLasy)
				{
					bind.NonLazy();
				}
			}
			RecursivelyBind(fiValue);
		}
	}

	private IEnumerable<FieldInfo> GetMonoBehavioursFields(MonoBehaviour root)
	{
		var result = new List<FieldInfo>();
		foreach (var fi in root.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic))
		{
			if (fi.FieldType.IsSubclassOf(typeof(MonoBehaviour)))
			{
				result.Add(fi);
			}
		}
		return result;
	}

	private InstallPrefabAttribute GetInstallPrefabAttribute(FieldInfo fi)
	{
		return fi.GetCustomAttribute<InstallPrefabAttribute>();
	}

	private void BindFactories()
	{
		Container.BindFactory<ProjectileTypes, Projectile, Projectile.Factory>().FromFactory<ProjectileFactory>();
		Container.BindFactory<UFO, UFO.Factory>().FromComponentInNewPrefab(gameData.UFO);
		Container.BindFactory<AsteroidTypes, Asteroid, Asteroid.Factory>().FromFactory<AsteroidFactory>();
	}

	private void BindPools()
	{
		for (int i = 0; i < gameData.Projectiles.Length; i++)
		{
			Projectile projectilePrefab = gameData.Projectiles[i];
			Container.BindMemoryPool<Projectile, Projectile.MemoryPool>().WithInitialSize(32).WithId(projectilePrefab.Type)
				.FromComponentInNewPrefab(projectilePrefab).UnderTransformGroup("ProjectilePool-" + projectilePrefab.Type);
		}
	}
}
