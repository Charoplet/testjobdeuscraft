public interface IState
{
	bool IsAdditionalState { get; }
	void Load();
	void Unload();
}

public interface IState<T> : IState
{
	void Load(T arg);
}