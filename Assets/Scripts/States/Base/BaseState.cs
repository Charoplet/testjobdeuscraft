using Zenject;
using UnityEngine;

public abstract class BaseState : MonoBehaviour, IState
{
	[SerializeField]
	private bool isAdditionalState;
	public bool IsAdditionalState { get { return isAdditionalState; } }

	[Inject]
	private BackSignal backSignal;
	[Inject]
	protected DiContainer container;

	public static BaseState CurrentState { get; protected set; }

	public virtual void Load()
	{
		backSignal += OnHardwareBackPress;
		if(!IsAdditionalState && CurrentState)
		{
			CurrentState.Unload();
		}
		CurrentState = this;
	}

	public virtual void Unload()
	{
		backSignal -= OnHardwareBackPress;
	}

	protected virtual void OnHardwareBackPress()
	{
		Unload();
	}
}

public abstract class BaseState<T> : BaseState, IState<T>
{
	public virtual void Load(T arg)
	{
		base.Load();
	}
}