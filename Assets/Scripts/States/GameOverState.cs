﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GameOverState : BaseState
{
	[Inject]
	private UIManager uiManager;

	private GameOverWindow gameOverWindow;

	public override void Load()
	{
		base.Load();
		gameOverWindow = uiManager.GetView<GameOverWindow>();
		gameOverWindow.Init(uiManager.GetView<HUD>().Score);
		gameOverWindow.Show();
	}

	public override void Unload()
	{
		base.Unload();
		gameOverWindow.Hide();
	}
}
