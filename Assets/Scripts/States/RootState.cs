﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class RootState : BaseState
{
	[SerializeField] [InstallPrefab(typeof(GameManager), true)]
	private GameManager gameManagerPrefab;
	[SerializeField] [InstallPrefab(typeof(UIManager), true)]
	private UIManager uiManagerPrefab;
	[SerializeField] [InstallPrefab(typeof(StartState), true)]
	private StartState startStatePrefab;

	[Inject]
	private StartState startState;

	public override void Load()
	{
		base.Load();
		startState.Load();
	}
}
