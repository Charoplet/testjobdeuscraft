﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class StartState : BaseState
{
	[SerializeField] [InstallPrefab(typeof(GameplayState), true)]
	private GameplayState gameplayStatePrefab;
	[Inject]
	private UIManager uiManager;

	private StartMenu startMenu;

	public override void Load()
	{
		base.Load();
		startMenu = uiManager.GetView<StartMenu>();
		startMenu.Show();
	}

	public override void Unload()
	{
		base.Unload();
		startMenu.Hide();
	}
}
