﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GameplayState : BaseState<Player>
{
	[SerializeField] [InstallPrefab(typeof(GameOverState))]
	private GameOverState gameOverStatePrefab;
	[SerializeField] [InstallPrefab(typeof(Player))]
	private Player playerPrefab;
	[SerializeField] [InstallPrefab(typeof(EnemySpawner))]
	private EnemySpawner enemySpawnerPrefab;
	[Inject]
	private UIManager uiManager;
	[Inject]
	private ObjectWasDestroyedSignal<Player> playerWasDestroyedSignal;
	[Inject]
	private GameOverState gameOverState;

	private Player player;
	private EnemySpawner enemySpawner;
	private HUD hud;

	public override void Load()
	{
		base.Load();
		// todo: use method injection??
		player = container.Resolve<Player>();
		enemySpawner = container.Resolve<EnemySpawner>();
		enemySpawner.StartSpawning();
		hud = uiManager.GetView<HUD>();
		hud.Init(0, player.LaserCurrentCount);
		hud.Show();
		playerWasDestroyedSignal += OnPlayerWasDestroyedSignal;
	}

	public override void Unload()
	{
		base.Unload();
		hud.Hide();
		playerWasDestroyedSignal -= OnPlayerWasDestroyedSignal;
	}

	private void OnPlayerWasDestroyedSignal()
	{
		gameOverState.Load();
	}
}
