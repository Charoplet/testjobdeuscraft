﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class UFO : FactoryObject<UFO>, IDestroyableObject
{
	[SerializeField]
	private Rigidbody2D rb;
	[SerializeField]
	private float speed = 10f;

	[Inject]
	private Player player;
	[Inject]
	private ObjectWasDestroyedSignal<UFO> ufoWasDestroyedSignal;

	private void FixedUpdate()
	{
		rb.velocity = player
			? (player.transform.position - transform.position).normalized * speed
			: Vector3.zero;		
	}

	//todo: Ufo and Asteroid have the same method - refactoring??
	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.CompareTag(Constants.Tags.Player))
		{
			var destroyable = collision.gameObject.GetComponent<IDestroyableObject>();
			if(destroyable != null)
			{
				destroyable.TakeDamage(DamageTypes.DestroyPlayer);
			}
		}
	}

	public void TakeDamage(DamageTypes damageType)
	{
		if(damageType == DamageTypes.Bullet || damageType == DamageTypes.Laser)
		{
			Destroy(gameObject);
			ufoWasDestroyedSignal.Fire();
		}
	}
}
