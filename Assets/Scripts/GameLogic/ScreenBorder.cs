﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EdgeCollider2D))]
public class ScreenBorder : MonoBehaviour
{
	//todo: handle changes of the screen!
	private void Start()
	{
		Rect screenRect = ScreenTools.GetScreenWorldRect(Camera.main);
		Vector2[] colliderPoints = 
		{
			new Vector2(screenRect.xMin, screenRect.yMin),
			new Vector2(screenRect.xMin, screenRect.yMax),
			new Vector2(screenRect.xMax, screenRect.yMax),
			new Vector2(screenRect.xMax, screenRect.yMin),
			new Vector2(screenRect.xMin, screenRect.yMin)
		};
		GetComponent<EdgeCollider2D>().points = colliderPoints;
	}
}
