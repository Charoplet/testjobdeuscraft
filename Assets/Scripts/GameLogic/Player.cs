﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Player : MonoBehaviour, IDestroyableObject
{
	[SerializeField]
	private Rigidbody2D rb;
	[SerializeField]
	private Transform firePlace;
	[SerializeField]
	private float speed = 10f;
	[SerializeField]
	private int bulletFireRate = 2;
	[SerializeField]
	private int laserFireRate = 2;
	[SerializeField]
	private int laserMaxCount = 5;
	[SerializeField]
	private float laserRestoreTime = 1f;
	[Inject]
	private Projectile.Factory projectileFactory;
	[Inject]
	private LaserCountChangedSignal laserCountChangedSignal;
	[Inject]
	private ObjectWasDestroyedSignal<Player> wasDestroyedSignal;

	private int laserCurrentCount;
	public int LaserCurrentCount
	{
		get { return laserCurrentCount; }
		set
		{
			laserCurrentCount = Mathf.Clamp(value, 0, laserMaxCount);
			laserCountChangedSignal.Fire(laserCurrentCount);
		}
	}

	private float bulletFireInterval;
	private float bulletNextFireTimer;

	private float laserFireInterval;
	private float laserNextFireTimer;
	private float laserNextRestoreTimer;

	private void Awake()
	{
		bulletFireInterval = 1f / bulletFireRate;
		laserFireInterval = 1f / laserFireRate;
		LaserCurrentCount = laserMaxCount;
	}

	private void Update()
	{
		Fire();
	}

	private void FixedUpdate()
	{
		Move();
		Rotate();
	}

	private void Move()
	{
		float horizontalAxis = Input.GetAxis(Constants.Axises.Horizontal);
		float verticalAxis = Input.GetAxis(Constants.Axises.Vertical);
		rb.velocity = new Vector2(horizontalAxis, verticalAxis) * speed;
	}

	private void Rotate()
	{
		Camera cam = Camera.main;
		float distanceToCamera = cam.transform.position.y - transform.position.y;
		Vector3 mousePosition = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, distanceToCamera));
		float angle = Mathf.Atan2(mousePosition.y - transform.position.y, mousePosition.x - transform.position.x) * Mathf.Rad2Deg - 90f; // minus 90 because ship looks up
		rb.rotation = angle;
	}

	private void Fire()
	{
		if (bulletNextFireTimer > 0)
		{
			bulletNextFireTimer -= Time.deltaTime;
		}
		if (laserNextFireTimer > 0)
		{
			laserNextFireTimer -= Time.deltaTime;
		}
		if (laserNextRestoreTimer > 0)
		{
			laserNextRestoreTimer -= Time.deltaTime;
		}
		else if (LaserCurrentCount < laserMaxCount)
		{
			LaserCurrentCount++;
			laserNextRestoreTimer = laserRestoreTime;
		}

		if (Input.GetMouseButton(0))
		{
			if (bulletNextFireTimer <= 0)
			{
				var bullet = projectileFactory.Create(ProjectileTypes.Bullet);
				bullet.transform.position = firePlace.transform.position;
				bullet.transform.rotation = firePlace.transform.rotation;
				bulletNextFireTimer = bulletFireInterval;
			}
		}
		else if (Input.GetMouseButton(1))
		{
			if (LaserCurrentCount > 0 && laserNextFireTimer <= 0)
			{
				var laser = projectileFactory.Create(ProjectileTypes.Laser);
				laser.transform.position = firePlace.transform.position;
				laser.transform.rotation = firePlace.transform.rotation;
				laserNextFireTimer = laserFireInterval;
				if (laserNextRestoreTimer <= 0)
				{
					laserNextRestoreTimer = laserRestoreTime;
				}
				LaserCurrentCount--;
			}
		}
	}

	public void TakeDamage(DamageTypes damageType)
	{
		if(damageType == DamageTypes.DestroyPlayer)
		{
			Destroy(gameObject);
			wasDestroyedSignal.Fire();
		}
	}
}
