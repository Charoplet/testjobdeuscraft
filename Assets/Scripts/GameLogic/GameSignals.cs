﻿using Zenject;

public class GameStartSignal : Signal<GameStartSignal> { }
public class GameQuitSignal : Signal<GameQuitSignal> { }
public class GamePauseSignal : Signal<GamePauseSignal, bool> { }
public class GameFocusSignal : Signal<GameFocusSignal, bool> { }
public class BackSignal : Signal<BackSignal> { }
public class LaserCountChangedSignal : Signal<LaserCountChangedSignal, int> { };
public class ObjectWasDestroyedSignal<T> : Signal<ObjectWasDestroyedSignal<T>> where T : IDestroyableObject  { };


