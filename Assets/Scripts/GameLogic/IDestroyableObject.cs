﻿
public interface IDestroyableObject
{
	void TakeDamage(DamageTypes damageType);
}
