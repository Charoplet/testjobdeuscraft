﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GameManager : MonoBehaviour
{
	[Inject]
	private BackSignal backSignal;
	[Inject]
	private GameStartSignal gameStartSignal;
	[Inject]
	private GamePauseSignal gamePauseSignal;
	[Inject]
	private GameFocusSignal gameFocusSignal;
	[Inject]
	private GameQuitSignal gameQuitSignal;
	[Inject]
	private RootState rootState;

	private void Start()
	{
		gameStartSignal.Fire();
		rootState.Load();
	}

	private void OnApplicationPause(bool pause)
	{
		gamePauseSignal.Fire(pause);
	}

	private void OnApplicationQuit()
	{
		gameQuitSignal.Fire();
	}

	private void OnApplicationFocus(bool focus)
	{
		gameFocusSignal.Fire(focus);
	}

	private void Update()
	{
		//todo: should we use Update for this??
		if (Input.GetKeyUp(KeyCode.Escape))
		{
			backSignal.Fire();
		}
	}
}
