﻿using System;
using Zenject;

public class ProjectileFactory : IFactory<ProjectileTypes, Projectile>
{
	[Inject]
	private DiContainer container;

	public Projectile Create(ProjectileTypes type)
	{
		var pool = container.ResolveId<Projectile.MemoryPool>(type);
		Projectile projectile = pool.Spawn();
		projectile.Pool = pool;
		return projectile;
	}
}
