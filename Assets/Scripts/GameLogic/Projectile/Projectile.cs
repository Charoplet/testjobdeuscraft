﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Projectile : PoolableFactoryObject<Projectile, ProjectileTypes>
{
	[SerializeField]
	private ProjectileTypes type;
	public ProjectileTypes Type { get { return type; } }
	[SerializeField]
	private float speed = 10f;
	[SerializeField]
	private DamageTypes damageType;

	public MemoryPool Pool { get; set; }

	private void Update()
	{
		transform.Translate(transform.up * Time.deltaTime * speed, Space.World);
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		var destroyable = collision.gameObject.GetComponent<IDestroyableObject>();
		if (destroyable != null)
		{
			destroyable.TakeDamage(damageType);
			Pool.Despawn(this);
		}
	}
}
