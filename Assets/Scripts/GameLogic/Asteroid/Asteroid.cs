﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Asteroid : FactoryObject<AsteroidTypes, Asteroid>, IDestroyableObject
{
	[SerializeField]
	private Rigidbody2D rb;
	[SerializeField]
	private AsteroidTypes type;

	[Inject]
	private Factory asteroidFactory;
	[Inject]
	private ObjectWasDestroyedSignal<Asteroid> asteroidWasDestroyedSignal;

	public void SetVelocity(Vector2 velocity)
	{
		rb.velocity = velocity;
	}

	//todo: Ufo and Asteroid have the same method - refactoring??
	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.CompareTag(Constants.Tags.Player))
		{
			var destroyable = collision.gameObject.GetComponent<IDestroyableObject>();
			if (destroyable != null)
			{
				destroyable.TakeDamage(DamageTypes.DestroyPlayer);
			}
		}
	}

	public void TakeDamage(DamageTypes damageType)
	{
		switch (damageType)
		{
			case DamageTypes.Laser:
				Destroy(gameObject);
				asteroidWasDestroyedSignal.Fire();
				break;
			case DamageTypes.Bullet:
				if (type == AsteroidTypes.Big)
				{
					//todo: move creation of asteroids into the EnemySpawner??
					Asteroid asteroidSmall1 = asteroidFactory.Create(AsteroidTypes.Small);
					Asteroid asteroidSmall2 = asteroidFactory.Create(AsteroidTypes.Small);
					//todo: don't spawn in the same place!
					asteroidSmall1.transform.position = asteroidSmall2.transform.position = transform.position;
					Vector2 velocity = rb.velocity * 3f;
					asteroidSmall1.SetVelocity(velocity);
					asteroidSmall2.SetVelocity(-velocity);
				}
				Destroy(gameObject);
				asteroidWasDestroyedSignal.Fire();
				break;
		}
	}
}
