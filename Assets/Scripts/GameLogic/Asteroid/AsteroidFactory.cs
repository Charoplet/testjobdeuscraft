﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class AsteroidFactory : IFactory<AsteroidTypes, Asteroid>
{
	[Inject]
	private GameData gameData;
	[Inject]
	private DiContainer container;

	public Asteroid Create(AsteroidTypes type)
	{
		switch (type)
		{
			case AsteroidTypes.Big:
				return container.InstantiatePrefabForComponent<Asteroid>(gameData.AsteroidsBig[Random.Range(0, gameData.AsteroidsBig.Length)]);
			case AsteroidTypes.Small:
				return container.InstantiatePrefabForComponent<Asteroid>(gameData.AsteroidsSmall[Random.Range(0, gameData.AsteroidsSmall.Length)]);
			default:
				return null;
		}
	}
}
