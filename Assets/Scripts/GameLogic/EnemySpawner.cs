﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class EnemySpawner : MonoBehaviour
{
	enum SpawnSides
	{
		Left,
		Top,
		Right,
		Bottom
	}

	[SerializeField]
	private float ufoSpawnInterval = 5f;
	[SerializeField]
	private float asteroidSpawnInterval = 2.5f;
	[SerializeField]
	private Vector2 asteroidSpeedInterval = new Vector2(0.1f, 1f);

	[Inject]
	private UFO.Factory ufoFactory;
	[Inject]
	private Asteroid.Factory asteroidFactory;
	[Inject]
	private ObjectWasDestroyedSignal<Player> playerWasDestroyedSignal;

	private bool canSpawnUfo;
	private bool canSpawnAsteroids;
	private Rect screenWorldRect;
	private Rect innerWorldRect;

	private void Awake()
	{
		screenWorldRect = ScreenTools.GetScreenWorldRect(Camera.main);
		var innerScreenSize = screenWorldRect.size * 0.66f;
		innerWorldRect = new Rect(screenWorldRect.center.x - innerScreenSize.x / 2f, screenWorldRect.center.y - innerScreenSize.y / 2f,
			innerScreenSize.x, innerScreenSize.y);
	}

	private void OnEnable()
	{
		playerWasDestroyedSignal += OnPlayerWasDestroyedSignal;
	}

	private void OnDisable()
	{
		playerWasDestroyedSignal -= OnPlayerWasDestroyedSignal;
	}

	public void StartSpawning()
	{
		canSpawnUfo = true;
		canSpawnAsteroids = true;
		StartCoroutine(SpawnUfoCoroutine());
		StartCoroutine(SpawnAsteroidCoroutine());
	}

	private IEnumerator SpawnUfoCoroutine()
	{
		while (canSpawnUfo)
		{
			UFO ufo = ufoFactory.Create();
			ufo.transform.position = GetRandomSidePosition();
			yield return new WaitForSeconds(ufoSpawnInterval);
		}
	}

	private IEnumerator SpawnAsteroidCoroutine()
	{
		while (canSpawnAsteroids)
		{
			Asteroid asteroid = asteroidFactory.Create(AsteroidTypes.Big);
			Vector2 position = GetRandomSidePosition();
			asteroid.transform.position = position;
			float angle = Random.Range(0, 360);
			asteroid.transform.Rotate(new Vector3(0, 0, angle));
			float scale = Random.Range(1f, 2f);
			asteroid.transform.localScale = new Vector3(scale, scale);
			Vector2 destPosition = GetRandomInnerPosition();
			float speed = Random.Range(asteroidSpeedInterval.x, asteroidSpeedInterval.y);
			asteroid.SetVelocity((destPosition - position).normalized * speed);
			yield return new WaitForSeconds(asteroidSpawnInterval);
		}
	}

	private Vector2 GetRandomSidePosition()
	{
		float xPos = 0f;
		float yPos = 0f;
		var side = EnumTools.GetRandomEnumValue<SpawnSides>();
		switch (side)
		{
			case SpawnSides.Right:
				xPos = screenWorldRect.xMin;
				yPos = Random.Range(screenWorldRect.yMin, screenWorldRect.yMax);
				break;
			case SpawnSides.Left:
				xPos = screenWorldRect.xMax;
				yPos = Random.Range(screenWorldRect.yMin, screenWorldRect.yMax);
				break;
			case SpawnSides.Top:
				xPos = Random.Range(screenWorldRect.xMin, screenWorldRect.xMax);
				yPos = screenWorldRect.yMax;
				break;
			case SpawnSides.Bottom:
				xPos = Random.Range(screenWorldRect.xMin, screenWorldRect.xMax);
				yPos = screenWorldRect.yMin;
				break;
		}
		return new Vector2(xPos, yPos);
	}

	private Vector2 GetRandomInnerPosition()
	{
		float xPos = Random.Range(innerWorldRect.xMin, innerWorldRect.xMax);
		float yPos = Random.Range(innerWorldRect.yMin, innerWorldRect.yMax);
		return new Vector2(xPos, yPos);
	}

	private void OnPlayerWasDestroyedSignal()
	{
		canSpawnAsteroids = canSpawnUfo = false;
		//todo: more clever way to destroy objects!
		foreach(var ufo in FindObjectsOfType<UFO>())
		{
			Destroy(ufo.gameObject);
		}
		foreach (var asteroid in FindObjectsOfType<Asteroid>())
		{
			Destroy(asteroid.gameObject);
		}
	}
}
