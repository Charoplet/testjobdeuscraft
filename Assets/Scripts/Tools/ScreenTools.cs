﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenTools
{
	public static Rect GetScreenWorldRect(Camera camera)
	{
		var leftBottom = camera.ScreenToWorldPoint(Vector3.zero);
		var rightTop = camera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height));
		return new Rect(leftBottom.x, leftBottom.y, rightTop.x - leftBottom.x, rightTop.y - leftBottom.y);
	}
}
