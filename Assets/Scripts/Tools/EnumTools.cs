﻿using System;
using Random = UnityEngine.Random;

public class EnumTools
{
	public static T GetRandomEnumValue<T>()
	{
		var values = Enum.GetValues(typeof(T));
		return (T)values.GetValue(Random.Range(0, values.Length));
	}
}
