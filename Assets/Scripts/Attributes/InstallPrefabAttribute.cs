﻿using System;

public class InstallPrefabAttribute : Attribute
{
	public Type Type { get; private set; }
	public bool NonLasy { get; private set; }

	public InstallPrefabAttribute(Type type, bool nonLazy = false)
	{
		Type = type;
		NonLasy = nonLazy;
	}
}
