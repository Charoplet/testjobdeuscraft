﻿using UnityEngine;

[CreateAssetMenu(fileName = "GameData", menuName = "GameData")]
public class GameData : ScriptableObject
{
	[SerializeField]
	private Projectile[] projectiles;
	public Projectile[] Projectiles { get { return projectiles; } }

	[SerializeField]
	private UFO ufo;
	public UFO UFO { get { return ufo; } }

	[SerializeField]
	private Asteroid[] asteroidsBig;
	public Asteroid[] AsteroidsBig { get { return asteroidsBig; } }

	[SerializeField]
	private Asteroid[] asteroidsSmall;
	public Asteroid[] AsteroidsSmall { get { return asteroidsSmall; } }
}
