﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class HUD : BaseView
{
	[SerializeField]
	private Text scoreText;
	[SerializeField]
	private Text laserCountText;

	[Inject]
	private LaserCountChangedSignal laserCountChangedSignal;
	[Inject]
	private ObjectWasDestroyedSignal<UFO> ufoWasDestroyedSignal;
	[Inject]
	private ObjectWasDestroyedSignal<Asteroid> asteroidWasDestroyedSignal;

	//todo: make ScoreManager to hold scores!
	private int score;
	public int Score
	{
		get { return score; }
		set
		{
			score = value;
			scoreText.text = score.ToString();
		}
	}

	public void Init(int score, int laserCount)
	{
		Score = score;
		laserCountText.text = laserCount.ToString();
	}

	public override void Show()
	{
		base.Show();
		laserCountChangedSignal += OnLaserCountChangedSignal;
		ufoWasDestroyedSignal += OnUfoWasDestroyedSignal;
		asteroidWasDestroyedSignal += OnAsteroidWasDestroyedSignal;
	}

	public override void Hide()
	{
		base.Hide();
		laserCountChangedSignal -= OnLaserCountChangedSignal;
		ufoWasDestroyedSignal -= OnUfoWasDestroyedSignal;
		asteroidWasDestroyedSignal -= OnAsteroidWasDestroyedSignal;
	}

	private void OnLaserCountChangedSignal(int laserCount)
	{
		laserCountText.text = laserCount.ToString();
	}

	//todo: make ScoreManager to hold scores!

	private void OnUfoWasDestroyedSignal()
	{
		Score += 200;
	}

	private void OnAsteroidWasDestroyedSignal()
	{
		Score += 100;
	}
}
