﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class GameOverWindow : BaseWindow
{
	[Inject]
	private GameplayState gameplayState;

	[SerializeField]
	private Text scoreText;

	public void Init(int score)
	{
		scoreText.text = score.ToString();
	}

	public void OnRepeatButtonClick()
	{
		gameplayState.Load();
	}

	public void OnExitButtonClick()
	{
		//todo: confirmation dialog!
		Application.Quit();
	}
}
