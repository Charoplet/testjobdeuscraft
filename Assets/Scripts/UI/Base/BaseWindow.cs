﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public abstract class BaseWindow : BaseView
{
	[SerializeField]
	private string title;
	public string Title { get { return title; } }
	[Inject]
	protected UIManager uiManager;

	private WindowTemplate winTemplate;

	public override void Show()
	{
		base.Show();
		winTemplate = uiManager.GetView<WindowTemplate>(true);
		winTemplate.Window = this;
		winTemplate.Show();
	}

	public override void Hide()
	{
		base.Hide();
		winTemplate.Window = null;
		winTemplate.Hide();
	}
}
