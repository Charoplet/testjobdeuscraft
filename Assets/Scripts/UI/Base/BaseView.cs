using UnityEngine;

public abstract class BaseView : MonoBehaviour
{
	public bool IsShowed { get { return gameObject.activeSelf; } }
	public bool DestroyOnHide { get; set; }

	public virtual void Show()
	{
		gameObject.SetActive(true);
	}

	public virtual void Hide()
	{
		if (DestroyOnHide)
		{
			Destroy(gameObject);
		}
		else
		{
			gameObject.SetActive(false);
		}
	}
}