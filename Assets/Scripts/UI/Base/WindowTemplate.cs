﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public sealed class WindowTemplate : BaseView
{
	[SerializeField]
	private Text titleText;
	[SerializeField]
	private RectTransform windowParent;

	private BaseWindow window;
	public BaseWindow Window
	{
		get { return window; }
		set
		{
			window = value;
			if (window)
			{
				window.transform.SetParent(windowParent.transform, false);
				titleText.text = window.Title;
			}
		}
	}

}
