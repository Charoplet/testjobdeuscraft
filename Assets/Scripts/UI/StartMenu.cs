﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class StartMenu : BaseView
{
	[Inject]
	private GameplayState gameplayState;

	public void OnPlayButtonClick()
	{
		gameplayState.Load();
	}
}
