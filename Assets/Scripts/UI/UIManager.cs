using System;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
	[SerializeField] private BaseView[] views;

	private Dictionary<Type, BaseView> viewsDict = new Dictionary<Type, BaseView>();

	public T GetView<T>(bool isCopy = false) where T : BaseView
	{
		var view = (T)viewsDict[typeof(T)];
		if (isCopy)
		{
			view = Instantiate(view);
			view.transform.SetParent(transform, false);
		}
		view.DestroyOnHide = isCopy;
		return view;
	}

	private void Awake()
	{
#if UNITY_EDITOR
		//if we want to work with UI manager in the dev scene, we must destroy it because there must be only one UIManager, instantiated from a prefab
		if (gameObject.scene.name == Constants.Dev.DevSceneName)
		{
			Destroy(gameObject);
			return;
		}
#endif

		foreach (var view in views)
		{
			viewsDict.Add(view.GetType(), view);
			view.gameObject.SetActive(false);
		}
	}
}